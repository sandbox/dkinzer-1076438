Welcome to simplenode.
--------------------------

The idea behind this module is to create a clean version of the node form.
This is useful in the cases where we would like to use the node form
as a simple and clean form for user input showing only necessary fields
and no extraneous fields that only complicate the end user experience.

This module is compatible the CCK module.  Thus, it provides
the same breadth of field types available to the CCK module.

All forms submitted via this module create a node of the same type as the 
node form used to create the form.

There will be no opportunity for the user to preview the content (at this point).
Thus, these forms are intended for one way communication.